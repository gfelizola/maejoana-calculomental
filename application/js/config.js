﻿Main.titulo = 'CÁLCULO MENTAL';

Main.tituloWithAudio = true;
Main.hasPreRun = true;

InterfaceController.mostrarGaleria     = false;
InterfaceController.mostrarBtnMenu     = false;
InterfaceController.mostrarBtnReinicar = true;
InterfaceController.mostrarBtnAjuda    = true;

InterfaceController.galeria = {};
InterfaceController.niveis = {};

var filesJSON = { "files": [ 
	{ "source": "system/css/img/sprite.png", "type": "IMAGE", "size": 22301  }
	// ,{ "sources": { "mp3" : { "source" : "application/assets/audios/cena2.10.mp3", "size" : 14976 } }, "type": "AUDIO"  },
	// { "sources": { "mp3" : { "source" : "application/assets/audios/cena2.1a.mp3", "size" : 10880 } }, "type": "AUDIO"  },
	// { "sources": { "mp3" : { "source" : "application/assets/audios/cena2.1b.mp3", "size" : 16512 } }, "type": "AUDIO"  },
	// { "sources": { "mp3" : { "source" : "application/assets/audios/cena2.1c.mp3", "size" : 12416 } }, "type": "AUDIO"  },
	// { "sources": { "mp3" : { "source" : "application/assets/audios/cena2.2a.mp3", "size" : 8320 } }, "type": "AUDIO"  },
	// { "sources": { "mp3" : { "source" : "application/assets/audios/cena2.2b.mp3", "size" : 19072 } }, "type": "AUDIO"  },
	// { "sources": { "mp3" : { "source" : "application/assets/audios/cena2.2c.mp3", "size" : 21632 } }, "type": "AUDIO"  },
	// { "sources": { "mp3" : { "source" : "application/assets/audios/cena2.3.mp3", "size" : 25728 } }, "type": "AUDIO"  },
	// { "sources": { "mp3" : { "source" : "application/assets/audios/cena2.4.mp3", "size" : 92288 } }, "type": "AUDIO"  },
	// { "sources": { "mp3" : { "source" : "application/assets/audios/cena2.5.mp3", "size" : 116352 } }, "type": "AUDIO"  },
	// { "sources": { "mp3" : { "source" : "application/assets/audios/cena2.6.mp3", "size" : 112256 } }, "type": "AUDIO"  },
	// { "sources": { "mp3" : { "source" : "application/assets/audios/cena2.7.mp3", "size" : 96896 } }, "type": "AUDIO"  },
	// { "sources": { "mp3" : { "source" : "application/assets/audios/cena2.8.mp3", "size" : 28288 } }, "type": "AUDIO"  },
	// { "sources": { "mp3" : { "source" : "application/assets/audios/cena2.9.mp3", "size" : 10880 } }, "type": "AUDIO"  },
	// { "sources": { "mp3" : { "source" : "application/assets/audios/cenaRelogio.mp3", "size" : 69248 } }, "type": "AUDIO"  },
	// { "sources": { "mp3" : { "source" : "application/assets/audios/titulo.mp3", "size" : 29727 } }, "type": "AUDIO"  },
	// { "sources": { "mp3" : { "source" : "application/assets/audios/trilha1.mp3", "size" : 4480722 } }, "type": "AUDIO"  },
	// { "sources": { "mp3" : { "source" : "application/assets/audios/trilha2.mp3", "size" : 4573742 } }, "type": "AUDIO"  },
	// { "sources": { "mp3" : { "source" : "application/assets/audios/trilha3.mp3", "size" : 4934218 } }, "type": "AUDIO"  },
	// { "sources": { "mp3" : { "source" : "application/assets/audios/trilha4.mp3", "size" : 4509973 } }, "type": "AUDIO"  },
	// { "sources": { "mp3" : { "source" : "application/assets/audios/trilha5.mp3", "size" : 3741927 } }, "type": "AUDIO"  }
] };