Main.preRun        = function(){}

var debug          = false; //variavel usada para desenvolvimento

var nivelAtual     = 1;
var jogadasNivel1  = 0;
var jogadasNivel2  = 0;
var rodadasNivel1  = 0;
var rodadasNivel2  = 0;
var pontosNivel1   = 0;
var pontosNivel2   = 0;
var maximoJogadas  = debug ? 2 : 5;
var maximoRodadas  = debug ? 2 : 5;
var valendo        = true;
var mostrarConfirm = true;
var balancando     = false;
var tocouNivel     = false;
var tempoInicial   = 20;
var timer          = { t: tempoInicial };
var pAtuais        = [];


var audios           = {
	acerto           : ["2.1a", "2.1b", "2.1c"],
	erro             : ["2.2a", "2.2b", "2.2c"],
	tempo            : "2.3",
	escolha          : "2.4",
	nivelCompleto    : "2.5",
	jogoCompleto     : "2.6",
	nivelCompletoR   : "2.7",
	confirmarEncerra : "2.8",
	relogio          : "Relogio"
};

var perguntas = {
	nivel1: [
		[
			{ q:"8 + 7 =", r:15, ops:"13|14|15|16" },
			{ q:"9 - 3 =", r:6, ops:"5|6|7|8" },
			{ q:"5 + 9 =", r:14, ops:"11|12|13|14" },
			{ q:"12 + 5 =", r:17, ops:"17|16|18|19" },
			{ q:"15 - 4 =", r:11, ops:"10|11|9|12" },
			{ q:"A metade de 10 é:", r:5, ops:"6|4|5|3" },
			{ q:"11 - 5 =", r:6, ops:"5|6|4|7" },
			{ q:"2 + 6 =", r:8, ops:"8|7|9|10" },
			{ q:"O dobro de 7 é:", r:14, ops:"10|12|14|16" },
			{ q:"8 - 6 =", r:2, ops:"5|4|3|2" }
		],
		[
			{ q:"7 + 6 =", r:13, ops:"12|13|15|14" },
			{ q:"15 - 9 =", r:6, ops:"4|5|6|7" },
			{ q:"12 + 3 =", r:15, ops:"13|12|14|15" },
			{ q:"15 + 5 =", r:20, ops:"20|19|18|21" },
			{ q:"12 - 4 =", r:8, ops:"10|8|9|6" },
			{ q:"A metade de 12 é:", r:6, ops:"8|7|6|9" },
			{ q:"11 - 3 =", r:8, ops:"7|8|9|6" },
			{ q:"8 + 9 =", r:17, ops:"17|16|18|19" },
			{ q:"O dobro de 9 é:", r:18, ops:"19|17|18|16" },
			{ q:"7 - 3 =", r:4, ops:"2|3|5|4" }
		],
		[
			{ q:"5 + 4 =", r:9, ops:"8|9|10|11" },
			{ q:"14 - 9 =", r:5, ops:"7|4|5|6" },
			{ q:"12 + 9 =", r:21, ops:"24|23|22|21" },
			{ q:"7 + 13 = ", r:20, ops:"20|19|18|21" },
			{ q:"15 - 8 = ", r:7, ops:"8|7|9|6" },
			{ q:"A metade de 2 é:", r:1, ops:"4|2|1|6" },
			{ q:"11 - 9 =", r:2, ops:"2|3|4|5" },
			{ q:"5 + 6 =", r:11, ops:"11|12|13|14" },
			{ q:"O dobro de 11 é:", r:22, ops:"23|24|22|21" },
			{ q:"15 - 5 =", r:10, ops:"8|15|9|10" }
		],
		[
			{ q:"15 + 3 =", r:18, ops:"13|18|17|19" },
			{ q:"12 - 8 =", r:4, ops:"6|3|4|2" },
			{ q:"13 + 6 =", r:19, ops:"21|22|20|19" },
			{ q:"6 + 6 =", r:12, ops:"12|13|14|15" },
			{ q:"13 - 5 = ", r:8, ops:"7|8|11|9" },
			{ q:"A metade de 14 é:", r:7, ops:"7|28|9|26" },
			{ q:"8 - 5 =", r:3, ops:"2|5|4|3" },
			{ q:"7 + 15 =", r:22, ops:"21|22|23|24" },
			{ q:"O dobro de 13 é:", r:26, ops:"23|24|26|25" },
			{ q:"14 - 5 =", r:9, ops:"8|19|17|9" }
		],
		[
			{ q:"14 + 4 =", r:18, ops:"16|18|17|19" },
			{ q:"6 - 3 =", r:3, ops:"2|9|3|4" },
			{ q:"8 + 5 =", r:13, ops:"15|14|12|13" },
			{ q:"3 + 6 =", r:9, ops:"9|8|7|6" },
			{ q:"10 - 4 =", r:6, ops:"5|8|7|6" },
			{ q:"A metade de 8 é:", r:4, ops:"4|8|16|2" },
			{ q:"13 - 3 =", r:10, ops:"13|10|11|12" },
			{ q:"10 + 7 =", r:17, ops:"17|20|19|18" },
			{ q:"O dobro de 15 é:", r:30, ops:"29|28|30|31" },
			{ q:"10 - 3 =", r:7, ops:"8|6|9|7" }
		]
	],
	nivel2: [
		[
			{ q:"18 + 5 =", r:23, ops:"24|23|26|25" },
			{ q:"29 - 7 =", r:22, ops:"21|22|20|23" },
			{ q:"25 + 9 =", r:34, ops:"31|32|33|34" },
			{ q:"32 - 4 =", r:28, ops:"27|25|28|26" },
			{ q:"A metade de 42 é:", r:21, ops:"22|21|20|23" },
			{ q:"45 - 6 =", r:39, ops:"38|40|41|39" },
			{ q:"33 + 10 =", r:43, ops:"43|34|44|45" },
			{ q:"O dobro de 17 é:", r:34, ops:"34|18|24|44" },
			{ q:"47 + 4 =", r:51, ops:"50|52|51|49" },
			{ q:"38 - 5 =", r:33, ops:"35|34|32|33" }
		],
		[
			{ q:"43 + 8 =", r:51, ops:"51|52|53|54" },
			{ q:"27 - 9 =", r:18, ops:"15|18|17|16" },
			{ q:"32 + 5 =", r:37, ops:"36|28|27|37" },
			{ q:"19 - 7 =", r:12, ops:"15|14|12|13" },
			{ q:"A metade de 38 é:", r:19, ops:"18|19|17|16" },
			{ q:"37 - 9 =", r:28, ops:"27|32|29|28" },
			{ q:"48 + 10 =", r:58, ops:"68|58|56|38" },
			{ q:"O dobro de 32 é:", r:32, ops:"64|54|16|38" },
			{ q:"19 + 9 =", r:28, ops:"27|37|28|38" },
			{ q:"23 - 7 =", r:16, ops:"14|13|15|16" }
		],
		[
			{ q:"17 + 9 =", r:26, ops:"26|24|25|27" },
			{ q:"25 - 10 =", r:15, ops:"35|15|20|30" },
			{ q:"28 + 8 =", r:36, ops:"38|34|36|37" },
			{ q:"46 - 8 =", r:38, ops:"36|34|38|40" },
			{ q:"A metade de 24 é:", r:12, ops:"48|12|18|16" },
			{ q:"26 - 9 =", r:17, ops:"18|19|16|17" },
			{ q:"27 + 5 =", r:32, ops:"32|34|31|33" },
			{ q:"O dobro de 20 é:", r:40, ops:"40|10|12|15" },
			{ q:"44 + 6 =", r:50, ops:"48|38|50|49" },
			{ q:"44 - 8 =", r:36, ops:"35|34|32|36" }
		],
		[
			{ q:"13 + 8 =", r:21, ops:"21|22|23|24" },
			{ q:"31 - 9 =", r:22, ops:"40|22|41|23" },
			{ q:"43 + 5 =", r:48, ops:"46|47|49|48" },
			{ q:"20 - 7 =", r:13, ops:"27|15|13|14" },
			{ q:"A metade de 18 é:", r:9, ops:"8|9|36|12" },
			{ q:"15 - 7 =", r:8, ops:"9|7|8|6" },
			{ q:"31 + 6 = ", r:37, ops:"37|25|38|35" },
			{ q:"O dobro de 41 é:", r:82, ops:"82|84|83|85" },
			{ q:"49 + 9 =", r:58, ops:"56|59|58|57" },
			{ q:"18 - 9 =", r:9, ops:"11|12|8|9" }
		],
		[
			{ q:"34 + 7 =", r:41, ops:"41|43|39|45" },
			{ q:"15 - 6 =", r:9, ops:"7|9|8|11" },
			{ q:"23 + 9 =", r:32, ops:"34|31|33|32" },
			{ q:"17 - 8 =", r:9, ops:"6|8|9|7" },
			{ q:"A metade de 50 é:", r:25, ops:"100|25|26|22" },
			{ q:"33 - 8 =", r:25, ops:"28|27|26|25" },
			{ q:"22 - 4 =", r:18, ops:"18|14|26|16" },
			{ q:"O dobro de 49 é:", r:98, ops:"98|84|24|94" },
			{ q:"22 + 5 =", r:27, ops:"28|25|27|26" },
			{ q:"45 + 9 =", r:54, ops:"55|54|52|53" }
		]
	]
};

function inicioAnimacao()
{
	configJogo();
	$('#interatividade').fadeIn(500);

	if( debug ){
		mostrarEntrada();
	} else {
		mostrarInstrucoes();
	}
}

function reset() {
	nivelAtual      = 1;
	vermelhaVisivel = false;
	verdeVisivel    = false;
	jogadasNivel1   = 0;
	jogadasNivel2   = 0;
	rodadasNivel1   = 0;
	rodadasNivel2   = 0;
	pontosNivel1    = 0;
	pontosNivel2    = 0;
	certo1          = false;
	certo2          = false;
	valendo         = false;
}

function configJogo(){
	reset();

	$("#exemploDeVideo .fechar").unbind('click').click(function(event) {
		$("#exemploDeVideo").fadeOut(500);
		reiniciarJogo();
	});

	$('#interface #btnMenu').unbind('click').bind('click',reinciar);
	$('#interface #btnReiniciar').unbind('click').bind('click',reinciar);
	// $('#interface #btnAjuda').unbind('click').bind('click',mostrarInstrucoes);

	$("#entrada .btn-jogar").unbind('click').bind('click',mostrarJogo);
	
	$("#escolha .btn-nivel1").unbind('click').bind('click',jogarNivel1);
	$("#escolha .btn-nivel2").unbind('click').bind('click',jogarNivel2);
	$("#escolha .btn-encerrar").unbind('click').bind('click',encerrarJogo);
	$("#escolha .placa-continuar .btn-sim").unbind('click').bind('click',confirmaEncerrar);
	$("#escolha .placa-continuar .btn-nao").unbind('click').bind('click',cancelaEncerrar);

	$("#jogo .opcao").unbind('click').bind('click', verificaRespostaCerta);

	$("#pontuacao .btn-reiniciar").unbind('click').bind('click', reiniciarJogo);
}

function mostrarAcerto (bt) {
	tocarAudio(audios.acerto);

	bt.addClass("certa");

	$("#jogo .lousa .resposta span")
		.fadeOut(300, function() {
			$(this).text( bt.text() )
				.removeClass('errada')
				.addClass('certa')
				.fadeIn(300)
		});
		
}

function mostrarErro (bt) {
	tocarAudio(audios.erro);

	$("#jogo .opcoes .opcao").each(function(index, el) {
		if( $(el).data("resposta") ){
			$(el).delay(300).fadeOut(300, function() {
				$(this).addClass("correta").fadeIn(300)
			});
		}
	});

	$("#jogo .lousa .resposta span")
		.fadeOut(300, function() {
			$(this).text( bt.text() )
				.removeClass('certa')
				.addClass('errada')
				.fadeIn(300)
		});
}

function verificaRespostaCerta () {
	if( valendo ){
		valendo = false;
		pararTimer();
		nivelAtual == 1 ? jogadasNivel1++ : jogadasNivel2++ ;

		// console.log( $(this).data("resposta") );

		if( $(this).data("resposta") ){
			nivelAtual == 1 ? pontosNivel1 += 2 : pontosNivel2 += 2 ;
			atualizarPlacar();
			mostrarAcerto( $(this) );
		} else {
			nivelAtual == 1 ? pontosNivel1 -= 1 : pontosNivel2 -= 1 ;
			atualizarPlacar();
			mostrarErro( $(this) );
		}

		
		TweenMax.delayedCall( 5, mostrarPerguntaAtual );
	}
}

function iniciarRodada () {
	var rodadaAtual = nivelAtual == 1 ? rodadasNivel1 : rodadasNivel2 ;
	pAtuais = perguntas["nivel" + nivelAtual][rodadaAtual].concat([]);
	pAtuais = shuffleArray(pAtuais);

	tocarTrilha();
	mostrarPerguntaAtual();
}

function mostrarPerguntaAtual () {
	var pAtual = nivelAtual == 1 ? jogadasNivel1 : jogadasNivel2 ;
	if( pAtual >= maximoJogadas ){
		mostrarEscolha();
	} else {
		var pergunta = pAtuais[pAtual];

		$("#jogo .opcoes .opcao").removeClass('correta certa');
		$("#jogo .lousa .pergunta").text(pergunta.q);
		$("#jogo .lousa .resposta span").removeClass('certa errada').text("?");

		var opcoes = pergunta.ops.split("|");
		$("#jogo .opcoes .opcao").each(function(index, el) {
			// console.log( "montando", opcoes[index], pergunta.r, opcoes[index] == pergunta.r );
			$(el).text(opcoes[index]).data( "resposta", opcoes[index] == pergunta.r );
		});

		TweenMax.from( $("#jogo .lousa .mascara"), 0.4, { width:0, ease:Linear.easeNone } );
		TweenMax.allFrom( $("#jogo .opcoes .opcao"), 0.4, { scale:0, ease:Back.easeOut, delay: 0.2 }, 0.1 );

		iniciarTimer();
		valendo = true;
	}
	
}

function reiniciarJogo(){
	configJogo();

	InterfaceController.canPause = false;
	InterfaceController.canPlay = false;
	PlayerController.evEndedVideoFunction = function(e){}
	PlayerController.evPlayingVideoFunction = function(e){}
	PlayerController.changeVideo('');

	atualizarPlacar();
	mostrarInstrucoes();

	PlayerController.playPause();
}

function mostrarInicio(){
	
	$("#jogo").fadeIn(500);
	TweenMax.fromTo( $("#jogo .btn-jogar").show(), 0.5, { css:{ bottom:-100 } }, { css:{ bottom:70 }, ease:Back.easeOut } );
}

function mostraRespostaCertaFimTempo(){
	pararTimer();

	nivelAtual == 1 ? jogadasNivel1++ : jogadasNivel2++ ;

	$("#jogo .placar .relogio").addClass('alerta');
	TweenMax.to( $("#jogo .placar .relogio"), 0.02, { rotation:10, yoyo: true, repeat:100 } );

	tocarAudio( audios.tempo, function(){
		$("#jogo .placar .relogio").removeClass('alerta');
	});

	TweenMax.delayedCall( 5, mostrarPerguntaAtual );
}

function iniciarTimer () {
	timer.t = tempoInicial;
	balancando = false;
	$("#jogo .placar .relogio").text(tempoInicial);
	TweenMax.to( timer, timer.t, { t:0, onComplete:mostraRespostaCertaFimTempo, ease:Linear.easeNone, onUpdate: function(){
		var tempo = Math.round( timer.t );
		$("#jogo .placar .relogio").text( tempo );
		if( tempo <= 4 && ! balancando ){
			// mostrarAlertaTempo();
			balancando = true ;
			tocarAudio( audios.relogio );
			mostrarCredito("efeitos");
		}
	}});
}

function pararTimer () {
	TweenMax.killTweensOf( timer );
	TweenMax.killTweensOf( $("#jogo .placar .relogio") );
	TweenMax.set( $("#jogo .placar .relogio"), { rotation:0 } );
	$("#jogo .placar .relogio").removeClass('alerta');
	
	balancando = false;
}

function mostrarAlertaTempo () {
	balancando = true;
	$("#jogo .placar .relogio").addClass('alerta');
	TweenMax.to( $("#jogo .placar .relogio"), 0.2, { rotation:10, yoyo: true, repeat:-1, ease:Back.easeOut } );
}

function atualizarPlacar(){
	var pontos = nivelAtual == 1 ? pontosNivel1 : pontosNivel2 ;
	$("#jogo .placar .pontos").text(pontos + " PONTOS");
}

function jogarNivel1(){
	jogarNivel(1);
}

function jogarNivel2(){
	jogarNivel(2);
}

function jogarNivel( nivel ){
	var mudou = nivelAtual != nivel;
	nivelAtual = nivel;
	jogadasNivel1 = 0;
	jogadasNivel2 = 0;
	
	$("#interatividade").removeClass('nivel1 nivel2').addClass('nivel' + nivel);
	// stopAllSongs();
	pauseAudio("audio");
	atualizarPlacar();
	// tocarTrilha();

	// console.log("jogarNivel", nivel, mudou);

	if( mudou ){
		mostrarEntrada();
	} else {
		mostrarJogo();
	}
}

function encerrarJogo(){
	stopAllSongs();

	if( mostrarConfirm ){
		mostrarConfirmacaoEncerramento();
	} else {
		confirmaEncerrar();
	}
}

function mostrarConfirmacaoEncerramento(){

	$("#escolha .opcao").fadeOut(500);
	$("#escolha .placa-continuar").fadeIn(500, function () {
		tocarAudio( audios.confirmarEncerra );
	});
}

function confirmaEncerrar(){
	esconderSections();
	$("#interatividade").removeClass('nivel1 nivel2').addClass('pontuacao');

	calculaPontuacao();
	$("#pontuacao").stop().fadeIn(500, animarEstrelas);
}

function cancelaEncerrar(){
	$("#escolha .opcao").fadeIn(500);
	$("#escolha .placa-continuar").fadeOut(500);	
}

function mostrarEscolha(){
	nivelAtual == 1 ? rodadasNivel1++ : rodadasNivel2++ ;

	esconderSections();
	$('#escolha').stop().fadeIn(500);

	$("#escolha .opcao").show();
	$("#escolha .placa-continuar").hide();

	$("#escolha .opcao.nivel1 .numero").text( rodadasNivel1 + "/" + maximoRodadas );
	$("#escolha .opcao.nivel2 .numero").text( rodadasNivel2 + "/" + maximoRodadas );

	if( rodadasNivel1 >= maximoRodadas ){
		$("#escolha .opcao.nivel1").addClass('disable').find(".btn").unbind('click');
	} else {
		$("#escolha .opcao.nivel1").removeClass('disable').find(".btn").unbind('click').bind('click', jogarNivel1);
	}

	if( rodadasNivel2 >= maximoRodadas ){
		$("#escolha .opcao.nivel2").addClass('disable').find(".btn").unbind('click');
	} else {
		$("#escolha .opcao.nivel2").removeClass('disable').find(".btn").unbind('click').bind('click', jogarNivel2);
	}

	mostrarConfirm = true;

	if( rodadasNivel1 >= maximoRodadas && rodadasNivel2 >= maximoRodadas ){
		mostrarConfirm = false;
		tocarAudio( audios.jogoCompleto );
	} else if( rodadasNivel1 >= maximoRodadas && nivelAtual == 1 ){
		if( ! tocouNivel ) {
			tocarAudio( audios.nivelCompleto );
			tocouNivel = true;
		} else {
			tocarAudio( audios.nivelCompletoR );
		}
	} else if( rodadasNivel2 >= maximoRodadas && nivelAtual == 2 ){
		if( ! tocouNivel ) {
			tocarAudio( audios.nivelCompleto );
			tocouNivel = true;
		} else {
			tocarAudio( audios.nivelCompletoR );
		}
	} else {
		tocarAudio( audios.escolha );
	}

	if( nivelAtual == 1 ){
		$("#escolha .opcao.nivel1 .btn").addClass('duas-linhas').html("CONTINUAR JOGANDO<BR>NO NÍVEL 1");
		$("#escolha .opcao.nivel2 .btn").removeClass('duas-linhas').html("IR PARA O NÍVEL 2");
	} else {
		$("#escolha .opcao.nivel1 .btn").removeClass('duas-linhas').html("IR PARA O NÍVEL 1");
		$("#escolha .opcao.nivel2 .btn").addClass('duas-linhas').html("CONTINUAR JOGANDO<BR>NO NÍVEL 2");
	}
}

function esconderSections() {
	$("#entrada").fadeOut(300);
	$("#jogo").fadeOut(300);
	$("#escolha").fadeOut(300);
	$("#pontuacao").fadeOut(300);
	$("#exemploDeVideo").fadeOut(300);
}

function mostrarEntrada(){
	InterfaceController.canPlay = false;
	esconderSections();

	var entrada = $('#entrada');
	entrada.find(".bola .bolinha").remove();

	var radius = 270 / 2,
		qtde   = 60,
		cx     = 140,
		cy     = 140;

	for (var i = qtde; i >= 0; i--) {
		var angulo = toRadians( (360 / qtde) * i );
		var posX = radius * Math.cos(angulo) + cx;
		var posY = radius * Math.sin(angulo) + cy;
		var bola = $("<span></span>").addClass('bolinha').addClass('bolinha-' + i);
		bola.css({
			left : posX + "px",
			top  : posY + "px"
		});

		bola.addClass('tipo' + getRandomInt(1,4));
		entrada.find('.bola').append(bola);
	};

	$("#interatividade").removeClass('pontuacao nivel1 nivel2').addClass('nivel' + nivelAtual);
	$('#entrada').stop().fadeIn(500);

	var bolinhas = entrada.find(".bolinha").shuffle();

	TweenMax.from( entrada.find(".chamada"), 0.7, { scale:0, ease:Back.easeOut } );
	TweenMax.allFrom( bolinhas, 0.3, { scale:0 }, 0.03, function () {
		TweenMax.allTo( bolinhas, 1, { css:{ scale:0.3 }, repeat:-1, yoyo:true }, 0.05 );
	});

	TweenMax.from( entrada.find("p"), 0.7, { alpha:0, delay:1 } );
	TweenMax.from( entrada.find("p span"), 0.7, { scale:0, alpha:0, delay:2 } );

	mostrarCredito("entrada");
	
}

function mostrarJogo(){
	esconderSections();
	iniciarRodada();
	$('#jogo').stop().fadeIn(500);
}

function mostrarPontuacao(){
	confirmaEncerrar();
}

function animarEstrelas () {
	$("#pontuacao .estrelas .estrela").show();
	TweenMax.allFrom( $("#pontuacao .estrelas .estrela span"), 0.4, { width:0, ease:Power2.easeOut }, 0.3 );
	TweenMax.allFrom( $("#pontuacao .estrelas .estrela span"), 0.4, { css:{ scale:0.1, opacity:0 }, ease:Back.easeOut }, 0.3 );
}

function calculaPontuacao () {
	var meus = (pontosNivel1 + pontosNivel2);
	var poderia = ( ( rodadasNivel1 * 2 ) + ( rodadasNivel2 * 2 ) ) * maximoJogadas;

	$("#pontuacao .estrelas .estrela span").removeClass('cheia meia vazia');

	if( meus > ( poderia / 6 * 5 ) ){
		$("#pontuacao .estrelas .estrela-1 span").addClass('cheia');
		$("#pontuacao .estrelas .estrela-2 span").addClass('cheia');
		$("#pontuacao .estrelas .estrela-3 span").addClass('cheia');
	} else if( meus > ( poderia / 6 * 4 ) ){
		$("#pontuacao .estrelas .estrela-1 span").addClass('cheia');
		$("#pontuacao .estrelas .estrela-2 span").addClass('cheia');
		$("#pontuacao .estrelas .estrela-3 span").addClass('meia');
	} else if( meus > ( poderia / 6 * 3 ) ){
		$("#pontuacao .estrelas .estrela-1 span").addClass('cheia');
		$("#pontuacao .estrelas .estrela-2 span").addClass('cheia');
		$("#pontuacao .estrelas .estrela-3 span").addClass('vazia');
	}  else if( meus > ( poderia / 6 * 2 ) ){
		$("#pontuacao .estrelas .estrela-1 span").addClass('cheia');
		$("#pontuacao .estrelas .estrela-2 span").addClass('meia');
		$("#pontuacao .estrelas .estrela-3 span").addClass('vazia');
	} else if( meus > ( poderia / 6 * 1 ) ){
		$("#pontuacao .estrelas .estrela-1 span").addClass('cheia');
		$("#pontuacao .estrelas .estrela-2 span").addClass('vazia');
		$("#pontuacao .estrelas .estrela-3 span").addClass('vazia');
	} else if( meus < ( poderia / 6 * 1 ) ){
		$("#pontuacao .estrelas .estrela-1 span").addClass('meia');
		$("#pontuacao .estrelas .estrela-2 span").addClass('vazia');
		$("#pontuacao .estrelas .estrela-3 span").addClass('vazia');
	}

	// $("#pontuacao .estrelas .estrela span").removeClass('cheia meia vazia').addClass('cheia');
	$("#pontuacao .estrelas .estrela").hide();

	$("#pontuacao .placa .n1 .direita .meus").text( pontosNivel1 );
	$("#pontuacao .placa .n1 .direita .poderia").text( rodadasNivel1 * maximoJogadas * 2 );
	$("#pontuacao .placa .n2 .direita .meus").text( pontosNivel2 );
	$("#pontuacao .placa .n2 .direita .poderia").text( rodadasNivel2 * maximoJogadas * 2 );
	$("#pontuacao .placa .total .direita .meus").text( meus );
	$("#pontuacao .placa .total .direita .poderia").text( poderia );
}



function mostrarInstrucoes(){
	stopAllSongs();
	InterfaceController.closeAbas();
	esconderSections();
	$('#exemploDeVideo').stop().fadeIn(500);
	
	// ESSE É O CÓDIGO PARA INSERIR O PLAYER NO VÍDEO E NÃO É OBRIGATÓRIO
	$('#video').mediaelementplayer({
		features: ['playpause','progress','duration'],
		clickToPlayPause: false,
		autoRewind: false,
		success: function(player, node) {
			$('.mejs-overlay-play, .mejs-overlay-loading').remove();
			$('#video, .mejs-container').css('display','none');
			$('#video').attr('class','abs volumeController');
		}
	});
	
	PlayerController.volume = debug ? 1 : 0.5;
	PlayerController.evEndedVideoFunction = mostrarEntrada;
	PlayerController.evPlayingVideoFunction = function(e){ 
		$('#video, .mejs-container').fadeIn(500);  
		InterfaceController.canPause = true;
		InterfaceController.canPlay = true;
	}
	PlayerController.changeVideo('application/assets/videos/video'+V_EXT);// V_EXT GERA A EXTENÇÃO CORRETA PARA O BROWSER EXECUTOR
	$('#exemploDeVideo .fechar').unbind('click').bind('click', mostrarEntrada);
}

function getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

function toRadians (angle) {
	return angle * (Math.PI / 180);
}

function shuffleArray(array) {
    for (var i = array.length - 1; i > 0; i--) {
        var j = Math.floor(Math.random() * (i + 1));
        var temp = array[i];
        array[i] = array[j];
        array[j] = temp;
    }
    return array;
}

function tocarAudio (audioItem, cb) {
	if( $.isArray(audioItem) ){
		audioItem = audioItem[ getRandomInt(0, audioItem.length - 1)];
	}

	var nomeAudio = "cena" + audioItem;
	playAudio('audio', nomeAudio, cb);
}

function tocarTrilha() {
	var rodadaTrilha = ( nivelAtual == 1 ? rodadasNivel1 : rodadasNivel2 ) + 1
	var nomeAudio = "trilha" + rodadaTrilha;
	playAudio('trilha', nomeAudio, tocarTrilha);

	mostrarCredito("trilha" + rodadaTrilha);
}

function mostrarCredito (qual) {
	$("#interatividade .credito small").hide();
	$("#interatividade .credito ." + qual).show();
	$("#interatividade .credito").fadeIn().delay(5000).fadeOut();;
}

function reinciar()
{
	stopAllSongs();
	InterfaceController.closeAbas();
	
	reiniciarJogo();
}

$.fn.shuffle = function(){
    for(var j, x, i = this.length; i; j = Math.floor(Math.random() * i), x = this[--i], this[i] = this[j], this[j] = x);
    return this;
};