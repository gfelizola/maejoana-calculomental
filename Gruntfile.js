'use strict';

module.exports = function(grunt) {
    require("matchdep").filterDev("grunt-*").forEach(grunt.loadNpmTasks);

    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),

        dirs: {
            base : ".",
            js   : "./application/js",
            css  : "./application/css",
            img  : "./application/assets/images"
        },

        // Server de DEV.
        connect: {
            server: {
                options: {
                    port: 9000,
                    livereload: true,
                    open: true
                }
            }
        },

        // Watch
        watch: {
           options: {
                livereload: true
            },
            css: {
                files: ["<%= dirs.css %>/*.css"]
            },
            js: {
                files: ["Gruntfile.js","<%= dirs.js %>/*.js","!<%= dirs.js %>/script.js"],
                tasks: ["concat"]
            },
            others: {
                files: ["<%= dirs.base %>/**/*.{html,txt}","<%= dirs.img %>/*.{gif,jpg,png}"]
            }
        },

        rsync: {
            options: {
                args: ["--verbose"],
                exclude: [".git*","*.scss","node_modules","Gruntfile.js","package.json","*.zip","*.DS_Store"],
                recursive: true
            },
            gustavo: {
                options: {
                    src: "./",
                    dest: "/var/www/html/projetos/ca",
                    host: "root@gustavofelizola.com"
                }
            },
        },

        release: {
            options: {
                npm: false,
                github: false
            }
        }
    });
    
    grunt.registerTask("w", ["connect","watch"]);
    grunt.registerTask("r", ["release", "rsync"]);
    
    grunt.registerTask("default", ["w"]);
};
