﻿PlayerController = 
{
	objPlayer: '',
	isPaused:true,
	isMuted:false,
	isEnded:false,
	duracao:0,
	volume:0,
	currentTime:0,
	canplaythrough:false,
	
	init:function()
	{
		var that = this;
		
		that.objPlayer = document.getElementById('video');

		if(that.objPlayer){
		
			$(that.objPlayer).unbind('play').bind('play',function(e){ 
				that.isPaused = false; 
				that.isEnded = false;
			});
			$(that.objPlayer).unbind('pause').bind('pause',function(e){ 
				that.isPaused = true; 
			});
			$(that.objPlayer).unbind('canplaythrough load').bind('canplaythrough load',evCanPlayTrough);
			$(that.objPlayer).unbind('volumechange').bind('volumechange',function(e){ that.volume = that.objPlayer.volume; });
			$(that.objPlayer).unbind('progress').bind("progress", function(e){ /*console.log('progress');*/ });
			$(that.objPlayer).unbind('playing').bind("playing",evPlayingVideo);
			$(that.objPlayer).unbind('waiting').bind("waiting", function(e){ /*console.log('waiting');*/ });
			$(that.objPlayer).unbind('ended').bind("ended",evEndedVideo);
		}
	},
	changeVideo: function(strVideo){
		var that = this;
		if(that.objPlayer){		
			$(that.objPlayer).attr('src',strVideo);
			//that.init();
			that.objPlayer.load();
			that.objPlayer.play();			
		}
	},
	play:function(val)
	{
		var that = this;		
		if(that.objPlayer){		
			that.objPlayer.pause();
			if(val){
				that.objPlayer.currentTime = val;
			}
			that.objPlayer.play();
		}
	},
	pause:function(val)
	{
		var that = this;
		if(that.objPlayer){
			that.objPlayer.pause();
			if(val){
				that.objPlayer.currentTime = val;
			}
		}
	},
	playPause:function()
	{
		var that = this;
		
		if(that.objPlayer){		
			if(that.isPaused){
				if(!Main.isInteractive){
					that.objPlayer.play();
				}
			}else{
				that.objPlayer.pause();
			}
			
		}
	},
	muteUnmute:function()
	{
		var that = this;
		if(that.isMuted){
			that.objPlayer.muted=false;
			$("#player #btVolume").attr('class','on');
			that.isMuted = false;
			that.updateMoveVolume(that.volume);
		}else{
			that.objPlayer.muted=true;
			$("#player #btVolume").attr('class','off');
			that.isMuted = true;
			that.updateMoveVolume(0);
		}
	},
	evEndedVideoFunction: function(e){
		//console.log('ended');
	},
	evPlayingVideoFunction: function(e){
		//console.log('playing');
	}
}

var evCanPlayTrough = function(e){
	PlayerController.canplaythrough = true;
};

var evEndedVideo = function(e){
	var that = this;
	that.isEnded = true;
	PlayerController.evEndedVideoFunction(e);
}

var evPlayingVideo = function(e){
	PlayerController.evPlayingVideoFunction(e);
}