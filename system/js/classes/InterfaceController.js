﻿var InterfaceController = {
	isMuted: false,
	galeria: [],
	idGaleria: 0,
	canPause: false,
	canPlay: false,
	canSlideGaleria: true,
	niveis: {
		tipo:'',
		itens: []
	},
	
	mostrarGaleria: false,	
	mostrarBtnMenu: false,
	mostrarBtnReinicar: false,
	mostrarBtnAjuda: false,
	mostrarBtnDivisaoPolitica: false,
	mostrarBtnVisaoSatelite: false,
	
	init: function(){
		var that = this;

		setVolume();
		$('#interface').fadeIn(500).css({overflow:'hidden'});
		
		// ABRE E FECHA A INTERFACE
		$('#interface #btnOpcoes').unbind('click').bind('click',function(e){
			e.stopPropagation();		
			var status = $('#interface').attr('data-status');
			if(status == 'off'){
				that.openAbas();		
				$(this).attr('title','FECHAR');		
			}else{				
				that.closeAbas();
				$(this).attr('title','OPÇÕES');
			}
		});
		
		// BOTÃO SOM
		if($('audio').size() == 0 && $('video').size() == 0){
			$('#interface #btnSom').remove();
		}else{
			if(Main.isTablet){
				$('#interface #btnSom').remove();
			}else{
				$('#interface #btnSom').unbind('click').bind('click',function(e){		
					if(!that.isMuted){
						mute();				
						that.isMuted = true;
						$(this).attr('data-status','off');
					}else{
						unmute();
						$(this).attr('data-status','on');	
						that.isMuted = false;
					}
					that.closeAbas();
				});
			}
		}
		
		// BOTÃO INFORMAÇÕES
		$("#interface #informacoes .viewport .overview").html(Textos.creditos);
		$('#interface #btnInformacoes').unbind('click').bind('click',function(e){
			var _status = $("#interface #informacoes").attr('data-status');
			
			if(_status == 'closed'){			
				$(this).addClass('hover');				
				$("#interface #informacoes").attr('data-status','open');
				TweenMax.to($("#interface #informacoes"),.5,{css:{left:55}, ease:Linear.ease, onStart: function(){ 
					$('#interface #informacoes').tinyscrollbar({invertscroll: true}); }});	

				$("#interface #ajuda .fechar").trigger('click');				
			}else{
				$("#interface #informacoes .fechar").trigger('click');
			}
		});
		$("#interface #informacoes .fechar").unbind('click').bind('click',function(e){
			TweenMax.to($("#interface #informacoes"),.5,{css:{left:-550}, ease:Linear.ease});
			$('#interface #btnInformacoes').removeClass('hover');
			$("#interface #informacoes").attr('data-status','closed');
		});
		
		// BOTÃO AJUDA
		$("#interface #ajuda .viewport .overview").html(Textos.ajuda);
		$('#interface #btnAjuda').unbind('click').bind('click',function(e){
			var _status = $("#interface #ajuda").attr('data-status');
			
			if(_status == 'closed'){			
				$(this).addClass('hover');				
				$("#interface #ajuda").attr('data-status','open');
				TweenMax.to($("#interface #ajuda"),.5,{css:{left:55}, ease:Linear.ease, onStart: function(){
					$('#ajuda').tinyscrollbar({invertscroll: true}); }});	
				
				$("#interface #informacoes .fechar").trigger('click');
			}else{
				$("#interface #ajuda .fechar").trigger('click');
			}
		});
		$("#interface #ajuda .fechar").unbind('click').bind('click',function(e){
			TweenMax.to($("#interface #ajuda"),.5,{css:{left:-550}, ease:Linear.ease});
			$('#interface #btnAjuda').removeClass('hover');
			$("#interface #ajuda").attr('data-status','closed');
		});
		
		// INICIAR A GALERIA DE IMAGENS
		that.iniciaGaleria();
		
		if(!InterfaceController.mostrarBtnMenu) $('#btnMenu').css('display','none');
		if(!InterfaceController.mostrarBtnReinicar) $('#btnReiniciar').css('display','none');
		if(!InterfaceController.mostrarBtnAjuda) $('#btnAjuda').css('display','none');
		
		// INICIAR ANIMAÇÃO
		inicioAnimacao();
	},
	openAbas: function(){			
		var that = InterfaceController;
		
		TweenMax.to($("#interface .b1"),.5,{css:{left:55}, ease:Linear.ease});
		TweenMax.to($("#interface .b2"),.5,{css:{top:55}, ease:Linear.ease, onStart: function(){
			$('#interface .filtro, #interface .corner').fadeIn(500);
			$('#interface').attr('data-status','on').css({overflow:'visible'});
		
			if(that.niveis.itens.length > 0){
				var _niveis = '', k;
				for(k = 0; k < that.niveis.itens.length; k++){
					if(typeof(that.niveis.itens[k]) == 'object'){
						_niveis += "<a href='javascript:;' class='niv_'>"+that.niveis.itens[k].texto+"</a>";
					}else{
						_niveis += that.niveis.itens[k];
					}
				}
				$('#interface .filtro').html("<nav class='niveis "+that.niveis.tipo+"'>"+_niveis+"</nav>");
				$("#interface .niveis").css({marginTop:-($("#interface .niveis").height() / 2)+"px"});
				$("#interface .niveis .niv_").each(function(j,el){
					$(el).unbind('click').bind('click',that.niveis.itens[j].acao);
				});
			}
		}, onComplete: function(){
			if(that.mostrarGaleria){
				TweenMax.to($("#interface .boxGaleria"),.5,{css:{top:55}, ease:Linear.ease});
			}
		}});
		if(that.canPause) PlayerController.pause();
	},
	closeAbas: function(){			
		var that = InterfaceController;
		
		TweenMax.to($("#interface .b1"),.5,{css:{left:-745}, ease:Linear.ease});
		TweenMax.to($("#interface .b2"),.5,{css:{top:-545}, ease:Linear.ease, onStart: function(){
			$('#interface .filtro, #interface .corner').fadeOut(500);
			$('#interface').attr('data-status','off')
			if(that.mostrarGaleria){
				TweenMax.to($("#interface .boxGaleria"),.25,{css:{top:-200}, ease:Linear.ease});
			}
		},onComplete: function(){
			if(that.canPlay) PlayerController.play();
			$('#interface').css({overflow:'hidden'});
		}});
		
		$("#interface #ajuda .fechar").trigger('click');
		$("#interface #informacoes .fechar").trigger('click');
	},
	iniciaGaleria: function(){
		var that = InterfaceController,
			i;
		if(that.galeria.length > 0){
			$('#interface .boxGaleria .viewport .overview').html('');
			$('#galeria').html('');
			for(i=0; i<that.galeria.length; i++){
				var g = that.galeria[i];
				
				$('#interface .boxGaleria .viewport .overview').append("<figure class='"+g.tipo+"' data-id='"+i+"'> <img src='"+g.imageTmb.src+"'> <span></span> <figcaption>"+g.imageTmb.titulo+"</figcaption> </figure>");
				
				if(g.tipo == 'image'){
					var _texto = '',
						_legenda = '';
					if(g.image.texto != ''){
						 _texto = "<div class='texto' data-status='closed'>"+g.image.texto+"</div> <a href='javascript:;' title='ABRIR/FECHAR TEXTO' class='btMaisMenos' data-id='"+i+"' data-status='mais'>ABRIR/FECHAR TEXTO</a> ";
					}
					if(g.image.legenda != ''){
						_legenda = "<div class='legenda'>"+g.image.legenda+"</div>";
					}
					$('#galeria').append("<section id='g"+i+"' data-tipo='"+g.tipo+"' class='abs galItens' style='left:800px'> <img src='"+g.image.src+"' class='abs' /> <h4>"+g.image.titulo+"</h4> <div class='credito'> <small>"+g.image.credito+"</small> </div> "+_texto+" "+_legenda+" </section>");
				}else if(g.tipo == 'video'){
					$('#galeria').append("<section id='g"+i+"' data-tipo='"+g.tipo+"' class='abs galItens' style='left:800px'> <video id='video_"+i+"' class='abs volumeController' width='800' height='600' preload='auto' src='"+g.video.src+"' controls='controls'></video> </section>");
				}				
			}
			$('#interface .boxGaleria .viewport .overview').css({width:(  ($('.boxGaleria .viewport .overview figure').width() + 50)  * that.galeria.length)+'px'});
			$('#interface .boxGaleria').tinyscrollbar({axis:'x', invertscroll: true});			
			$('#interface .boxGaleria figure').unbind('click').bind('click',function(e){ 
				var _id = $(this).attr('data-id');
				that.showGaleria(_id);				
			});
			
			$('#galeria').append("<a href='javascript:;' title='FECHAR' class='fechar'>FECHAR</a> <a href='javascript:;' title='ANTERIOR' class='stNvGaleria stPrev'>ANTERIOR</a> <a href='javascript:;' title='PRÓXIMO' class='stNvGaleria stNext'>PRÓXIMO</a>");
			
			$('#galeria > section > .btMaisMenos').unbind('click').bind('click',that.textoGaleria);
			$('#galeria .stNext').unbind('click').bind('click',that.nextGaleria);
			$('#galeria .stPrev').unbind('click').bind('click',that.prevGaleria);
			$('#galeria .fechar').unbind('click').bind('click',that.closeGaleria);
			
		}else{
			$('#interface .boxGaleria').css('display','none');
		}
	},
	openFeedback: function(id,callback){
		$(id).fadeIn(500,callback);	
		var w = parseInt($(id+' section').width()),
			h = parseInt($(id+' section').height());
		$(id+' section').css({
			'margin-top':-(h / 2)+'px',
			'margin-left':-(w / 2)+'px'
		});
	},
	showGaleria: function(id){		
		var that = InterfaceController;
		that.closeAbas();
		that.idGaleria = parseInt(id);
		
		$('#galeria .stPrev, #galeria .stNext, #galeria .fechar').css({display:'block'});
		if(that.idGaleria == 0){
			$('#galeria .stPrev').css({display:'none'});
		}
		if(that.idGaleria >= (that.galeria.length-1)){
			$('#galeria .stNext').css({display:'none'});
		}
		
		$('#galeria .galItens').css('left','800px');
		$('#galeria #g'+id).css({left:0});
		$('#galeria').fadeIn(500);
		
		$('#galeria .galItens').each(function(k,el){
			var legHeight = $(el).children('.legenda').height();
			if($(el).children('.btMaisMenos').size() > 0){
				$(el).children('.btMaisMenos').css({bottom:legHeight + 30});
			}
			if(legHeight <= 0){
				$(el).children('.btMaisMenos').css({bottom:0});
			}
		});
		
		if(that.galeria[that.idGaleria].tipo == 'video'){
			that.loadPlayerVideo();
		}
		that.canPlay = false;
	},
	loadPlayerVideo:function(){
		var that = InterfaceController;
		
		$('#video_'+that.idGaleria).mediaelementplayer({
			features: ['playpause','progress','duration','volume'],
			clickToPlayPause: false,
			autoRewind: false,
			success: function(player, node) {
				$('.mejs-overlay-play, .mejs-overlay-loading').remove();
				player.play();
			}
		});
	},
	prevGaleria: function(){
		var that = InterfaceController,
			prev = that.idGaleria-1;
		if(that.canSlideGaleria){
			that.canSlideGaleria = false;
			if(prev >= 0){
				TweenMax.to($('#galeria #g'+that.idGaleria),.5,{css:{left:800}, ease:Linear.ease, onStart:function(){					
					if(that.galeria[that.idGaleria].tipo == 'video'){
						var vid = document.getElementById('video_'+that.idGaleria);
						vid.pause();
					}
				}, onComplete:function(){					
					if(that.galeria[that.idGaleria].tipo == 'video'){
						var vid = document.getElementById('video_'+that.idGaleria);
						vid.currentTime = 0;
					}
				}});
				TweenMax.fromTo($('#galeria #g'+prev),.5,{css:{left:-800}},{css:{left:0}, onComplete: function(){ 
					that.idGaleria = prev; 
					if(prev <= 0){
						$('#galeria .stPrev').css({display:'none'});
					}					
					if(that.galeria[that.idGaleria].tipo == 'video'){
						that.loadPlayerVideo();
					}					
					that.canSlideGaleria = true;
				}, ease:Linear.ease});
				$('#galeria .stNext').css({display:'block'});
			}
		}
	},
	nextGaleria: function(){
		var that = InterfaceController,
			next = that.idGaleria+1;
		if(that.canSlideGaleria){
			that.canSlideGaleria = false;
			if(next <= (that.galeria.length-1)){
				TweenMax.to($('#galeria #g'+that.idGaleria),.5,{css:{left:-800}, ease:Linear.ease, onStart:function(){					
					if(that.galeria[that.idGaleria].tipo == 'video'){
						var vid = document.getElementById('video_'+that.idGaleria);
						vid.pause();
					}
				}, onComplete:function(){					
					if(that.galeria[that.idGaleria].tipo == 'video'){
						var vid = document.getElementById('video_'+that.idGaleria);
						vid.currentTime = 0;
					}
				}});
				TweenMax.fromTo($('#galeria #g'+next),.5,{css:{left:800}},{css:{left:0}, onComplete: function(){ 
					that.idGaleria = next; 
					if(next >= (that.galeria.length-1)){
						$('#galeria .stNext').css({display:'none'});
					}					
					if(that.galeria[that.idGaleria].tipo == 'video'){
						that.loadPlayerVideo();
					}					
					that.canSlideGaleria = true;
				}, ease:Linear.ease});
				$('#galeria .stPrev').css({display:'block'});
			}
		}
	},
	closeGaleria: function(){
		var that = InterfaceController;
		that.canPlay = true;
		if(that.galeria[that.idGaleria].tipo == 'video'){
			var vid = document.getElementById('video_'+that.idGaleria);
			vid.pause();
			vid.currentTime = 0;
		}
		$('#galeria').fadeOut(500);
		that.callBackCloseGaleria();
	},
	callBackCloseGaleria: function(){},
	textoGaleria: function(){
		var that = InterfaceController,
			$bt = $(this),
			$texto = $('#galeria #g'+that.idGaleria+' .texto');
		
		if($texto.attr('data-status') == 'closed'){
			$bt.attr('data-status','menos');
			$texto.attr('data-status','opened');
			TweenMax.to($bt,.5,{css:{right:230}, ease:Linear.ease});
			TweenMax.to($texto,.5,{css:{right:0}, ease:Linear.ease, onStart: function(){
				$('#galeria .stNvGaleria, #galeria .fechar').fadeOut(250);
			}});
		}else{
			$bt.attr('data-status','mais');
			$texto.attr('data-status','closed');
			TweenMax.to($bt,.5,{css:{right:-2}, ease:Linear.ease, delay:.1});
			TweenMax.to($texto,.5,{css:{right:-265}, ease:Linear.ease, onStart: function(){
				$('#galeria .stNvGaleria, #galeria .fechar').fadeIn(500);
				if(that.idGaleria >= (that.galeria.length-1)){
					$('#galeria .stNext').css({display:'none'});
				}else if(that.idGaleria <= 0){
					$('#galeria .stPrev').css({display:'none'});
				}
			}});
		}
	}
}
