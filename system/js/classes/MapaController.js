﻿var MapaController = {
	idAbas: [
		{ id:'abaLateral', idOpen:'openAbaL', anima:{ on: { par:'left', val:'608' }, off: { par:'left', val:'792' } } },
		{ id:'abaInferior', idOpen:'openAbaI', anima:{ on: { par:'top', val:'565' }, off: { par:'top', val:'593' } } }
	],
	init: function(){
		var that = this;
		$('.abas').fadeIn().each(function(i,elem){
			$('#'+that.idAbas[i].idOpen).unbind('click').bind('click',function(){ MapaController.show(i) });
		});
		$('.abas > section').each(function(){
			var $ul = $(this).children('ul');
			$ul.children('li').each(function(index, domEle){
				var a = $(domEle).attr('data-aba'), 
					o = $(domEle).attr('data-opt'), 
					anular = $(domEle).attr('data-anular'),
					isblack = $(domEle).attr('data-isblack');
				$(domEle).unbind('click').bind('click',function(){
					if($(domEle).attr('class') == 'off'){
						if(anular){ 
							$('.aba'+a).css({ 'display':'none' });
							$('#aba'+a+' ul li').removeClass('on').addClass('off');
						}
						if(isblack){ 
							$('#layerDegrade').css({ 'display':'block' });
						}
						$('#mp'+a+'_opt'+o).css({ 'display':'block' });
						$(domEle).removeClass('off').addClass('on');
					}else{
						$('#mp'+a+'_opt'+o).css({ 'display':'none' });
						$(domEle).removeClass('on').addClass('off');
						if(isblack){ 
							MapaController.hideBlack(a);
						}
					}
				});
			});
		});
		$('#aba1').removeClass('off').addClass('on');
		$('#aba1 header, #aba2 header').css('cursor','pointer');
		$('#aba1 header').unbind('click').bind('click',function(){
			$(this)
			$('#aba2').removeClass('on').addClass('off');
			$('#aba2 ul').slideUp();
			$('#aba1 ul').slideDown();
			$('#aba2 ul li').removeClass('on').addClass('off');
			$('.aba2').css({ 'display':'none' });
			$('#aba1').removeClass('off').addClass('on');			
		});
		$('#aba2 header').unbind('click').bind('click',function(){
			$('#aba1').removeClass('on').addClass('off');
			$('#aba1 ul').slideUp();
			$('#aba2 ul').slideDown();
			$('#aba1 ul li').removeClass('on').addClass('off');
			$('.aba1').css({ 'display':'none' });
			$('#aba2').removeClass('off').addClass('on');			
		});
		
		if($('#fontesMapa').size() > 0){
			TweenMax.to($('#fontesMapa'), .5, {css:{ "opacity" : "1" }, ease:Linear.ease, onComplete: function(){
				setTimeout(function(){
					var i = 0, l = MapaController.idAbas.length;
					for( i = 0; i < l; i++){
						MapaController.show(i);
					}
				 },3000);
			} });
		}
	},
	show: function(idx){
		var that = this;
		eval("TweenMax.to($('#'+that.idAbas[idx].id), .5, {css:{ "+that.idAbas[idx].anima.on.par+" : "+that.idAbas[idx].anima.on.val+" }, ease:Linear.ease });");
		$('#'+that.idAbas[idx].id).removeClass('off').addClass('on');	
		$('#'+that.idAbas[idx].idOpen).unbind('click').bind('click',function(){ MapaController.hide(idx) });
		$('#'+that.idAbas[idx].idOpen+' span').removeClass('off').addClass('on');	
		if(that.idAbas[idx].id == 'abaLateral'){
			if(Main.isTablet){
				$("#mapaInterativo").css({ 'left':'-90px' });
			}else{
				TweenMax.to($("#mapaInterativo"), .5, {css:{ 'left':'-90px' }, ease:Linear.ease });
			}
		}else if(that.idAbas[idx].id == 'abaInferior'){
			if(Main.isTablet){
				$("#mapaInterativo").css({ 'top':'-20px' });
			}else{
				TweenMax.to($("#mapaInterativo"), .5, {css:{ 'top':'-20px' }, ease:Linear.ease });
			}
		}
	},
	hide: function(idx){
		var that = this;
		eval("TweenMax.to($('#'+that.idAbas[idx].id), .5, {css:{ "+that.idAbas[idx].anima.off.par+" : "+that.idAbas[idx].anima.off.val+" }, ease:Linear.ease });");
		$('#'+that.idAbas[idx].id).removeClass('on').addClass('off');	
		$('#'+that.idAbas[idx].idOpen).unbind('click').bind('click',function(){ MapaController.show(idx) });
		$('#'+that.idAbas[idx].idOpen+' span').removeClass('on').addClass('off');	
		if(that.idAbas[idx].id == 'abaLateral'){
			if(Main.isTablet){
				$("#mapaInterativo").css({ 'left':'0' });
			}else{
				TweenMax.to($("#mapaInterativo"), .5, {css:{ 'left':'0' }, ease:Linear.ease });
			}
		}else if(that.idAbas[idx].id == 'abaInferior'){
			if(Main.isTablet){
				$("#mapaInterativo").css({ 'top':'0' });
			}else{
				TweenMax.to($("#mapaInterativo"), .5, {css:{ 'top':'0' }, ease:Linear.ease });
			}
		}
	},
	hideBlack: function(a){
		var t = $('.abas section ul li.on[data-isBlack=true]').size();
		t += (($('#politico').css('display') == 'none')?0:1);
		if(t <= 0 ){
			$('#layerDegrade').css({ 'display':'none' });
		}
	}
}