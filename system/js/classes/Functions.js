﻿var Functions = {
	strip_tags: function (str, allowed_tags){
		var key = '', allowed = false, matches = [], allowed_array = [], allowed_tag = '', i = 0, k = '', html = ''; 
		var replacer = function (search, replace, str){return str.split(search).join(replace);};
		if(allowed_tags){allowed_array = allowed_tags.match(/([a-zA-Z0-9]+)/gi);}
		str += ''; 
		matches = str.match(/(<\/?[\S][^>]*>)/gi);
		for(key in matches){
			if(isNaN(key)){continue;}
			html = matches[key].toString();
			allowed = false; 
			for(k in allowed_array){
				allowed_tag = allowed_array[k];
				i = -1; 
				if(i != 0){i = html.toLowerCase().indexOf('<'+allowed_tag+'>');}
				if(i != 0){i = html.toLowerCase().indexOf('<'+allowed_tag+' ');}
				if(i != 0){i = html.toLowerCase().indexOf('</'+allowed_tag);} 
				if(i == 0){allowed = true;break;}
			}
			 if(!allowed){str = replacer(html,"",str);}
		}
		 return str;
	},
	verificaBrowser: function(){
		var ua = navigator.userAgent.toLowerCase();
		if(ua.indexOf('chrome')!=-1){/** SE É CHROME*/
			BROWSER = 'Chrome';
			V_EXT = '.mp4';
			A_EXT = '.mp3';
		}else if(ua.indexOf('android')!=-1){/** SE É ANDROID*/
			BROWSER = 'Android';
			V_EXT = '.mp4';
			A_EXT = '.mp3';
		}else if(ua.indexOf('ipad')!=-1){/** SE É IPAD*/
			BROWSER = 'iPad';
			V_EXT = '.mp4';
			A_EXT = '.mp3';
		}else if(ua.indexOf('safari')!=-1){/** SE É SAFARI*/
			BROWSER = 'Safari';
			V_EXT = '.mp4';
			A_EXT = '.mp3';
		}else if(ua.indexOf('firefox')!=-1){/** SE É FIREFOX*/
			BROWSER = 'Firefox';
			V_EXT = '.mp4';
			A_EXT = '.ogg';
		}else if(ua.indexOf('opera')!=-1){/** SE É OPERA*/
			BROWSER = 'Opera';
			V_EXT = '.webm';
			A_EXT = '.ogg';
		}
	},
	updateScale: function(){
		var w = $(window).width(), h = $(window).height(), scaleFinal = 1, scaleX = w / Main._stage_w, scaleY = h / Main._stage_h;
		if(scaleX < 1 && scaleY < 1){
			if(scaleX < scaleY) scaleFinal = scaleX;
			else scaleFinal = scaleY;
		}else{
			if(scaleX < 1) scaleFinal = scaleX;
			else if(scaleY < 1) scaleFinal = scaleY;
		}	
		$('#stage').css({'transform':'scale('+scaleFinal+','+scaleFinal+')'}).attr('data-scale',scaleFinal);
	},
	dndStartFunction: function(ui){
		ui.position.left = 0;
		ui.position.top = 0;
	},
	dndDragFunction: function(ui){
		var scale = Number($('#stage').attr('data-scale'));
		var changeLeft = ui.position.left - ui.originalPosition.left; // find change in left
		var newLeft = ui.originalPosition.left + changeLeft / scale; // adjust new left by our zoomScale

		var changeTop = ui.position.top - ui.originalPosition.top; // find change in top
		var newTop = ui.originalPosition.top + changeTop / scale; // adjust new top by our zoomScale

		ui.position.left = newLeft;
		ui.position.top = newTop;
	},
	getOffset: function (evt){
		if(evt.offsetX!=undefined)
			return {x:evt.offsetX,y:evt.offsetY};
		 
		var el = evt.target;
		var offset = {x:0,y:0};
		 
		while(el.offsetParent)
		{
			offset.x+=el.offsetLeft;
			offset.y+=el.offsetTop;
			el = el.offsetParent;
		}
		 
		offset.x = evt.pageX - offset.x;
		offset.y = evt.pageY - offset.y;
		 
		return offset;
	}
}